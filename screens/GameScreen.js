import React, { useState } from 'react'
import { Text, StyleSheet, View, Alert } from 'react-native'
import NumberContainer from '../components/game/NumberContainer'
import PrimaryButton from '../components/ui/PrimaryButton'
import Title from './../components/ui/Title'

function generateRandomBetween(min, max, exclude) {
    const rndNum = Math.floor(Math.random() * (max - min)) + min
    
    if (rndNum == exclude) {
        return generateRandomBetween(min, max, exclude)
        } else {
        return rndNum
}
}

let minBoundary = 1
let maxBoundary = 100
function GameScreen({ userNumber }) {
    const initialGuess = generateRandomBetween(
        minBoundary,
        maxBoundary,
        userNumber,
)

const [currentGuess, setCurrentGuess] = useState(initialGuess)

function nextGuessHandler(direction) {
    if ((direction === 'lower' && currentGuess < userNumber) ||
    (direction === 'greater' && currentGuess > userNumber) ) {
    Alert.alert('Don\'t lie!', 'You know that this is wrong...', [
        {text: 'Sorry', style: 'cancel'}
    ])
    return
    }

    if (direction === 'lower' ){
        maxBoundary = currentGuess
    }
    else {
        minBoundary = currentGuess + 1
    }
    console.log(minBoundary, maxBoundary)
    const newRndNumber = generateRandomBetween(
        minBoundary,
        maxBoundary,
        currentGuess,
    )

    setCurrentGuess(newRndNumber)
    }
    return (
        <View style={styles.screen}>
        <Title>Opponent's Guess</Title>
        <NumberContainer>{currentGuess}</NumberContainer>
    {/* {Guess} */}
    <View>
        <Text>Higher or lower?</Text>
        <PrimaryButton onPress={nextGuessHandler.bind(this, 'greater')}>
            +
        </PrimaryButton>
        <PrimaryButton onPress={nextGuessHandler.bind(this, 'lower')}>
        -
        </PrimaryButton>
    </View>
    <View>{/* <View>Log rounds</View> */}</View>
</View>
)
}

export default GameScreen

const styles = StyleSheet.create({
    screen: {
    flex: 1,
    padding: 35,
    },
})
